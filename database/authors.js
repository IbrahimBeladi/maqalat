const authors = [
  {
    id: 'ibrahimbeladi',
    name: 'Ibrahim Beladi',
    major: 'CS',
    batch: 2012,
    job: 'Frontend Developer'
  }
]

export default authors
