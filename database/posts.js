const posts = [
  {
    id: 1,
    slug: 'ibrahimbeladi/posts/best-language',
    title: 'افضل لغة في العالم',
    author: 'ibrahimbeladi',
    created_at: '2021-11-26',
    updated_at: '2021-12-04'
  },
  {
    id: 2,
    slug: 'ibrahimbeladi/posts/my-experience',
    title: 'تجربتي',
    author: 'ibrahimbeladi',
    created_at: '2021-07-23',
    updated_at: '2021-12-04'
  },
  {
    id: 3,
    slug: 'ibrahimbeladi/posts/1000-languages',
    title: 'انا بتعلم 1000 لغة',
    author: 'ibrahimbeladi',
    created_at: '2021-11-25',
    updated_at: '2021-11-25'
  },
  {
    id: 4,
    slug: 'ibrahimbeladi/posts/native-or-not',
    title: 'نيتف ولا لا',
    author: 'ibrahimbeladi',
    created_at: '2021-09-17',
    updated_at: '2021-09-17'
  },
  {
    id: 5,
    slug: 'ibrahimbeladi/posts/oria-preoria-tips',
    title: 'نصايح عامة للاوريات والبريات',
    author: 'ibrahimbeladi',
    created_at: '2021-07-23',
    updated_at: '2022-06-17'
  },
  {
    id: 6,
    slug: 'ibrahimbeladi/posts/questions',
    title: 'اسئلة واجوبة',
    author: 'ibrahimbeladi',
    created_at: '2021-07-23',
    updated_at: '2022-06-17'
  },
  {
    id: 7,
    slug: 'ibrahimbeladi/posts/to-c-or-not-to-c',
    title: 'اتعلم سي ولا سي++؟',
    author: 'ibrahimbeladi',
    created_at: '2021-09-16',
    updated_at: '2021-09-16'
  },
  {
    id: 7,
    slug: 'ibrahimbeladi/posts/what-should-I-learn',
    title: 'ايش اتعلم قبل ما ادخل الجامعة',
    author: 'ibrahimbeladi',
    created_at: '2022-07-14',
    updated_at: '2022-07-14'
  }
]

export default posts
